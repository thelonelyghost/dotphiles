#!/usr/bin/env bash
#
# dotphiles : https://github.com/dotphiles/dotphiles
#
# Bootstrap dotphiles
#
# Authors:
#   Ben O'Hara <bohara@gmail.com>
#   David Alexander <davidpaulalexnder@gmail.com>
#

set -e

GITHUB_USERNAME=$1
DOTPHILES=$2

usage()
{
  echo
  echo "Usage: $0 github-username [.dotphiles]"
  echo
}

if [[ -z "$GITHUB_USERNAME" ]]; then
  echo "Need github username!"
  usage
  exit 1
fi

if [[ -z "$DOTPHILES" ]]; then
  DOTPHILES=".dotfiles"
fi

UPSTREAM="https://github.com/dotphiles/dotphiles"
ORIGIN="git@github.com:$GITHUB_USERNAME/dotphiles.git"

if [[ ! -d ~/$DOTPHILES ]]; then
  if [[ ! $(which git) ]]; then
    echo
    echo
    echo "ERROR:"
    echo "Git must be installed to proceed."
    echo "http://git-scm.com/downloads"
    echo
    exit 1
  fi
  echo "Cloning $ORIGIN to ~/$DOTPHILES"
  git clone --recursive $ORIGIN ~/$DOTPHILES
  cd ~/$DOTPHILES
  git remote add upstream $UPSTREAM
  echo
  echo "***************************************************"
  echo 'Done!'
fi

# From: http://stackoverflow.com/a/15391933
if [ "$(uname)" == "Darwin" ]; then
  platform="Mac OS X"
  platform_specific_installer="~/$DOTPHILES/deploy/osx"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  platform="GNU/Linux"
  platform_specific_installer="~/$DOTPHILES/deploy/linux"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
  platform="Cygwin"
  platform_specific_installer=''
fi

echo
echo "1. Edit ~/$DOTPHILES/dotsyncrc to enable dotfiles"
if [[ ! -z "$platform_specific_installer" ]]; then
  echo "2. Execute '$platform_specific_installer' (Platform: $platform)"
  echo '3. Symlink dotfiles into place with `dotsync -L`'
else
  echo '2. Symlink dotfiles into place with `dotsync -L`'
fi
echo

