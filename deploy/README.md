deploy
======

Scripts to deploy a new machine

osx
---

Setup OSX with sensible default options

### packages/macports

Install required packages using macports

### packages/brew

Install required packages using homebrew

### packages/pip

Install required packages using pip

linux
-----

Setup Linux with sensible default options

### packages/apt

Install required packages using apt

### packages/pip

Install required packages using pip
