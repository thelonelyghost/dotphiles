proprietary
===========

This directory shall host many git repositories that contain proprietary code used to
augment the environment. Any repositories cloned into this directory will:

1. be ignored by git automatically
2. have a directory named `bin/` at the repository root added to the system PATH

This means code not intended for public consumption may have its repository cloned
into subdirectories of this one. Those subdirectories should be full git repositories
and *not* submodules, or else they may be made public by mistake.

To use, add the following to **dotsyncrc**

    [files]
    ..
    proprietary
    ..
    [endfiles]

