# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
#window_root "~/Projects/rails-server"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "rails-server"

# Split window into panes.
split_h 70 # newly created pane is 70% of total width

# Pane 0
run_cmd "bundle exec rails s" 1

# Pane 1
run_cmd "echo '' > log/development.log" 2
run_cmd "echo '' > log/test.log" 2
send_keys "ngrok start rails" 2

# Set active pane.
select_pane 1

