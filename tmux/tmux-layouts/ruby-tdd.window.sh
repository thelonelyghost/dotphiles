# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
#window_root "~/Projects/editor"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "ruby-tdd"

# Split window into panes.
split_h 30

send_keys "vim ./" 1
send_keys "[[ -f \"./Guardfile\" ]] && ( [[ -f \"./Gemfile.lock\" ]] && bundle exec guard start --no-interactions --clear || \guard start --no-interactions --clear )" 2

# Set active pane
select_pane 1
