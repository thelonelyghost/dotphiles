#
# dotphiles : https://github.com/dotphiles/dotphiles
#
# Defines environment variables.
#
# Authors:
#   Ben O'Hara <bohara@gmail.com>
#

# Dont read global configs
unsetopt GLOBAL_RCS

# Set the path to dotzsh.
export DOTZSH="$HOME/.zsh"

# Source environment settings
source "$DOTZSH/env.zsh"

# Customize to your needs...

if [[ -d "$HOME/.tmuxifier/bin" ]]; then
  PATH="$HOME/.tmuxifier/bin:$PATH"

  export TMUXIFIER_LAYOUT_PATH="$HOME/.tmux-layouts"
  [[ ! -d "$TMUXIFIER_LAYOUT_PATH" ]] && mkdir -p "$TMUXIFIER_LAYOUT_PATH"

  eval "$(tmuxifier init -)"
fi

if [[ -d "$HOME/.local/bin" ]]; then
  PATH="$HOME/.local/bin:$PATH"
fi

if [[ -d "$HOME/.composer/vendor/bin" ]]; then
  PATH="$HOME/.composer/vendor/bin:$PATH"
fi

if [[ -d "$HOME/.rbenv/bin" ]]; then
  PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi

if [[ -d "$HOME/.bin" ]]; then
  PATH="$HOME/.bin:$PATH"
fi

if [[ -d "$HOME/.proprietary" ]]; then
  # add all subdirectories containing a directory named 'bin' to PATH
  # EXAMPLE: '~/.proprietary/derp/bin' and '~/.proprietary/bin' would be added, but
  #          '~/.proprietary/foo/bar/bin' would not.
  PATH="$(find -L $HOME/.proprietary -maxdepth 2 -mindepth 1 -name 'bin' -type d -printf '%p:')$PATH"
fi

if [[ -d "$HOME/.crystal/bin" ]]; then
  PATH="$HOME/.crystal/bin:$PATH"
fi

export GOPATH="$HOME/.go"
mkdir -p "$GOPATH/"{src,bin,pkg}
if [[ -d "$GOPATH/bin" ]]; then
  PATH="$GOPATH/bin:$PATH"
fi

if [[ -f "$HOME/.nvm/nvm.sh" ]]; then
  source $HOME/.nvm/nvm.sh
fi

# https://github.com/thoughtbot/suspenders/pull/282
# Git repositories can be whitelisted by running `whitelist_repo`
PATH=".git/safe/../../bin:$PATH"
PATH=".svn/safe/../../bin:$PATH"

export PATH
export DISABLE_AUTO_TITLE=true
